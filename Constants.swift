//
//  Constants.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/30/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//

import Foundation
import Cocoa

// list of constants used throughout the code
let OLIGOLENGTH = 19
let kDefaultFont = NSFont(name: "Courier", size: 12)
let kDefaultFontColor = NSColor.blackColor()
let kHighlightFont = NSFont(name: "Courier-Bold", size: 12)
let kHighlightFontColor = NSColor.redColor()
let kDesignedOligosFont =  NSFont(name: "Courier", size: 11)
let kEnsemblURL = "https://rest.ensembl.org/sequence/"