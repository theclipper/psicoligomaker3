//
//  FastaImporter.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/29/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//

import Cocoa
// An instance of this class can take a URL of a putative Fasta file, checks it is actually a fasta file, and if so returns a dictionary array containing the sequence name, and the sequence itself.

struct Fasta {
    var sequence: String = ""
    var name: String = ""
}

class FastaImporter: NSObject {
    
    var fasta = Fasta()
    // open the file at url, checks that it is a fasta file by testing that it has two lines and the first begins with '>'. returns an array containing the name of the sequence stripped of > and the sequence
    func importFastaFile(url: NSURL) -> Fasta{
        do {
            let sequence = try String(contentsOfURL: url)
            return self.parseFasta(sequence)
        }
        catch {
            print ("caught an error")
            let fasta = Fasta()
            return fasta //returns an empty fasta
        }
    }
    
    private func parseFasta (fileContent: String) -> Fasta{
        //checks that the file is a fasta file and parses it into a Fasta struc
        if (!fileContent.hasPrefix(">")) {
            //print ("Not a fasta file")
            return self.parseText(fileContent) //consider it a plain text sequence
        } else {
            let myLines = fileContent.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
            var fasta = Fasta()
            fasta.name = myLines[0].substringFromIndex(myLines[0].startIndex.successor()) //removes the ">"
            fasta.sequence=myLines[1...myLines.count-1].joinWithSeparator("").cleanSequence()
            return fasta
        }
    }
    
    private func parseText (fileContent:String) -> Fasta{
        var fasta = Fasta()
        fasta.sequence = fileContent.cleanSequence()
        return Fasta()
    }

}
