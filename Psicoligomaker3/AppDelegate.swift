//
//  AppDelegate.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/20/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

