//
//  MainViewController.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/20/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//

import Cocoa
import Foundation


class MainViewController: NSViewController, NSTableViewDelegate {
    //    let mySeq = Sequence(seq: "", seqType: "")
    // var mySeq: Sequence
    //MARK: Properties and Outlets
    var importer = FastaImporter()
    dynamic var oligoArray = NSMutableArray()
    dynamic var sequence = String()
    dynamic let possibleScores=["1","2","3","4","5","6","7","8","9", "10"]
    dynamic var minimumScore = "6"
    dynamic var sequenceName = ""
    dynamic var sequenceIdentifier = ""
    dynamic var topLeftOverhang = "T"
    dynamic var bottomLoop = "TTCAAGAGA".complement()
    dynamic var topLoop = "TTCAAGAGA" {
        didSet{
            self.bottomLoop = topLoop.complement()
        }
    }
    dynamic var topRightOverhang = "TTTTTTC"
    dynamic var bottomLeftOverhang = "A"
    dynamic var bottomRightOverhang = "AAAAAAGAGCT"
    @IBOutlet weak var OligoWindow: NSWindow!
    @IBOutlet var oligosTextView: NSTextView!
    @IBOutlet  var mainTextView: NSTextView!
    @IBOutlet weak var oligoArrayController: NSArrayController!
    @IBOutlet weak var minimumScorePopUp: NSPopUpButton!
    @IBOutlet weak var tableView: NSTableView!
    @IBOutlet weak var sequenceIdentifierTextView: NSTextField! //not used yet at this stage. But it may come handy later as an outlet
    @IBOutlet weak var settingsPanel: NSPanel!
    @IBOutlet weak var downloadFastaPanel: NSPanel!
   
    //MARK: IBActions
    @IBAction func setPsicoDefaults(sender: AnyObject) {
        sequenceIdentifier = ""
        topLeftOverhang = "T"
        topLoop = "TTCAAGAGA"
        topRightOverhang = "TTTTTC"
        bottomLeftOverhang = "A"
        bottomRightOverhang = "AAAAAAGAGCT"
    }
    @IBAction func startSearch(sender: NSObject){
        // using the sequence in the main text view, looks for oligos and calculates their score.
        let sequence = self.mainTextView.string!.uppercaseString.cleanSequence()
        self.mainTextView.string = sequence
        if (sequence.characters.count < OLIGOLENGTH) {
            self.showAlert("Sequence is too short", informativeText: "The sequence must be at least 19nt long")
            return
        }
        self.resetOligos()
        let shFinder = ACEShFinder(sequence, self.sequenceName)
        self.oligoArray = shFinder.findSh()
        
        if (self.oligoArray.count > 0 ) {
            self.filterOligos(self)
        } else {print ("Empty Array")}
        
    }
    
    @IBAction func openDownloadFastaWindow(sender: AnyObject) {
        self.downloadFastaPanel.makeKeyAndOrderFront(sender)
    }
    @IBAction func clickedDownloadSequence(sender: AnyObject) {
        if self.downloadFastaPanel.visible { self.downloadFastaPanel.close() }
        self.downloadFasta(self.sequenceIdentifier, identifierType: "id")
    }
    
    
    @IBAction func filterOligos(sender: AnyObject) {
        //called when the user changes the value of the minimum score popup
        //filter the array based on a minimum score set by the user
        let filterPredicate = NSPredicate(format: "score >=\(minimumScore)")
        self.oligoArrayController.filterPredicate=filterPredicate
    }
    
    @IBAction func clickImportFasta(sender: AnyObject) {
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = true
        panel.canCreateDirectories = false
        panel.beginSheetModalForWindow(self.view.window!, completionHandler: { (result) -> Void in if result == NSFileHandlingPanelOKButton{
            self.importFasta(panel.URL!)
            }
        })
    }
    
    @IBAction func openSettingsPanel(sender: AnyObject) {
        self.settingsPanel.makeKeyAndOrderFront(sender)
    }
    @IBAction func closeSettingsPanel(sender: AnyObject) {
        self.settingsPanel.close()
    }
    
    @IBAction func designOligos(sender: NSButton) {
        let oligoDesigner = OligoDesigner(topLeftOverhang: self.topLeftOverhang, topRightOverhang: self.topRightOverhang, topLoop: self.topLoop, bottomLoop: self.bottomLoop, bottomLeftOverhang: self.bottomLeftOverhang, bottomRightOverhang: self.bottomRightOverhang)
        var oligoPairArray = Array<OligoPair>()
        for oligo in self.oligoArrayController.selectedObjects {
            let newOligoPair: OligoPair = oligoDesigner.createOligoPair(oligo as! Oligo)
            oligoPairArray.append(newOligoPair)
        }
        self.displayDesignedOligoPairs(oligoPairArray)
    }
    
    @IBAction func closeOligoWindow(sender: AnyObject) {
        self.oligosTextView.window!.setIsVisible(false)
    }
    @IBAction func saveSequence(sender: AnyObject) {
        // saves the sequence in the mainTextView as a fasta file
        let panel = NSSavePanel()
        panel.canCreateDirectories = true
        panel.nameFieldStringValue = self.sequenceName
        panel.beginWithCompletionHandler({ (result) -> Void in
            if result == NSFileHandlingPanelOKButton{
                //let fileManager = NSFileManager.defaultManager()
                let string = ">\(self.sequenceName)\n\(self.mainTextView.string)"
                do {try string.writeToURL(panel.URL!.URLByAppendingPathExtension("fa"), atomically: true, encoding: NSUTF8StringEncoding)}
                catch {self.showAlert("Error", informativeText: "Couldn't save the file at \(panel.URL!)")}
            }
        })
    }
    
    @IBAction func saveToDisk(sender: AnyObject) {
        // saves the designed oligos to disk
        //let window = self.view.window
        let panel = NSSavePanel()
        panel.canCreateDirectories = true
        panel.beginWithCompletionHandler({ (result) -> Void in
            if result == NSFileHandlingPanelOKButton{
                //let fileManager = NSFileManager.defaultManager()
                do {try self.oligosTextView.string?.writeToURL(panel.URL!.URLByAppendingPathExtension("txt"), atomically: true, encoding: NSUTF8StringEncoding)}
                catch {self.showAlert("Error", informativeText: "Couldn't save the file at \(panel.URL!)")}
            }
        })
    }
    
    @IBAction func displayOligoWindow(sender: AnyObject) {
        //displays the content of the window
        if !self.oligosTextView.window!.visible{
        self.oligosTextView.window!.makeKeyAndOrderFront(self)
        self.oligosTextView.sizeToFit()
        self.oligosTextView.window!.setIsVisible(true)
        }
    }
    
    //MARK: private functions
    // instantiates an importer, imports the fasta file at the URL, and places the sequence in the main text window
    private func importFasta(url: NSURL){

        let fasta = self.importer.importFastaFile(url)
        if (fasta.sequence != "") {
            self.sequenceName = fasta.name
            self.mainTextView.string = fasta.sequence
            self.resetOligos()
        }
    }
    
    private func displayDesignedOligoPairs(oligoPairArray: Array <OligoPair>){
        // clean the contents of the oligosTextView, writes the newly designed oligo
        // and displays the window
        //self.oligosTextView.textContainer!.widthTracksTextView    =   false
        
        self.oligosTextView.string = "Name\tSequence (5'->3')\tScore"
        for oligo in oligoPairArray {
            self.oligosTextView.string = self.oligosTextView.string! + "\n" + oligo.tableSummaryTwoLines()
        }
        self.oligosTextView.textStorage!.font = kDesignedOligosFont
        self.displayOligoWindow(self)
    }
    private func highlightSelectedOligos(oligo: Oligo){
        // changes the color of sequence corresponding to the selected oligo(s) to red and bold
        let range = NSRange(location: oligo.position, length: OLIGOLENGTH)
        self.mainTextView.setTextColor(NSColor.redColor(), range: range)
        self.mainTextView.setFont(NSFont(name: "Courier-Bold", size: 12)!,  range: range)
        self.mainTextView.textStorage?.addAttribute(NSFontAttributeName, value: NSFont(name: "Courier-Bold", size: 12)!, range: range)
        let attributes: [String: AnyObject] = [NSFontAttributeName: kHighlightFont!, NSForegroundColorAttributeName: kHighlightFontColor]
        self.mainTextView.textStorage?.addAttributes(attributes, range: range)
    }
    
    private func resetDefaultAttributes(){
        let color = NSColor.blackColor()
        self.mainTextView.textStorage?.font=kDefaultFont
        self.mainTextView.textColor=color
    }
    
    
    //downloads fasta sequence from ensemble
    private func downloadFasta(identifier: String = "", identifierType: String = "id") {
        let url = NSURL(string: "\(kEnsemblURL)\(identifierType)/\(identifier)?content-type=text/x-fasta")!
        let sharedSession = NSURLSession.sharedSession()
        let downloadTask = sharedSession.downloadTaskWithURL(url, completionHandler: {(location: NSURL?, urlResponse: NSURLResponse?, error: NSError?) -> Void in
            if error != nil{
                dispatch_async(dispatch_get_main_queue(), {
                    self.showAlert("Download Error", informativeText: "Failed to connect to rest.ensembl.org")
                })
                return
            }
            do {
                let fileContent = try NSString(contentsOfURL: location!, encoding: NSUTF8StringEncoding) // check the file has been downloaded!
                
                if fileContent.hasPrefix("{\"error"){ // the searched id doesnt exist
                    dispatch_async(dispatch_get_main_queue(), {
                        let informativeText = fileContent.stringByReplacingOccurrencesOfString("{", withString: "").stringByReplacingOccurrencesOfString("\"", withString: "").stringByReplacingOccurrencesOfString("}", withString: "")
                        self.showAlert("Download Error", informativeText: informativeText)
                    })
                    return
                }
                // if all the error checks pass the test, then you
                let fasta = self.importer.importFastaFile(location!)
                dispatch_async(dispatch_get_main_queue(), {
                    self.sequence = fasta.sequence // get the sequence!
                    self.mainTextView.string = self.sequence
                    if fasta.name != "" {
                        self.sequenceName = fasta.name
                    } else{
                        // if the name cannot be inferred from the content of the file, use the provided identifier but alert the user.
                        self.sequenceName = "<?>" + identifier + "<?>"
                    }
                    self.resetOligos()
                })
            }
            catch {
                // if for any other reason it fails
                self.showAlert("Error!", informativeText: "Couldn't download the requested sequence")
            }
        })
        downloadTask.resume()
    }
    
    private func resetOligos(){
        self.resetDefaultAttributes()
        self.tableView.deselectAll(self)
        self.oligoArray.removeAllObjects()
        self.oligoArrayController.rearrangeObjects()
    }
    
    private func showAlert(messageText: String, informativeText: String){
        let alertPanel = NSAlert()
        alertPanel.alertStyle = NSAlertStyle.WarningAlertStyle
        alertPanel.messageText = messageText
        alertPanel.informativeText = informativeText
        alertPanel.runModal()
    }
    
    
    //MARK: Delegate functions and overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSBundle.mainBundle().URLForResource("test_sequence", withExtension: "")
        let fasta = self.importer.importFastaFile(url!)
        self.mainTextView.string = fasta.sequence
        self.sequenceName = fasta.name
        let font = NSFont(name: "Courier", size: 12)
        self.mainTextView.textStorage?.font = font
        self.oligosTextView.textStorage!.font = kDefaultFont
        
        // Do view setup here.
        let style = NSMutableParagraphStyle()
        style.defaultTabInterval = 44
        style.tabStops = Array()
        self.oligosTextView.defaultParagraphStyle = style
        
        
    }
    
    func tableViewSelectionDidChange(notification: NSNotification) {
        let selectedOligos = self.oligoArrayController.selectedObjects
        self.resetDefaultAttributes()
        if selectedOligos.count < 20{ // to prevent the program from stalling!
        for oligo in selectedOligos{
            self.highlightSelectedOligos(oligo as! Oligo)
            //scrolls the window so the selected target sequence is visible.
            // if more oligos are selected, scrolls to last selected oligo
            let range = NSMakeRange(oligo.position, OLIGOLENGTH)
            self.mainTextView.scrollRangeToVisible(range)
        }
        }
    }
    
    
    
}

