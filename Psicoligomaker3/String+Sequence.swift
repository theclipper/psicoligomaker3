//
//  String+Sequence.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/29/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//
// Adds DNA sequence manipulation funcionality to Strings

import Foundation


extension String{
    
    //checks whether a given string is composed exclusively of one of the four bases.
    func isDNA() -> Bool{
        let DNA_bases = Set<Character> (arrayLiteral: "A", "C", "G", "T")
        for char in self.uppercaseString.characters{
            if !DNA_bases.contains(char){
                return false
            }
        }
        return true
    }
    
    // returns a string with reverse orientation
    func reverse()-> String{
        var result = String()
        for char in self.characters{
            result = String(char) + result
        }
        return result
    }
    
    // returns a string that is the complement of complement
    func complement() ->String {
        let s = self.uppercaseString //convert to uppercase first
        let translation = ["A": "T", "T": "A", "C": "G", "G": "C"]
        var result = String()
        for char in s.characters{ // check if it is a valid base/
            if let val = translation[String(char)]{
                result = result + val
            } else{
                result = result + String(char)
            }
        }
        return result
    }
    
    //reverse + complement
    func revCom() ->String{
        return self.reverse().complement()
    }
    
    
    func cleanSequence() -> String{
        //returns a new string generate by removing every occerrence ofnew lines and white spaces
        var string = self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let components = string.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).filter { !$0.isEmpty }
        string =  components.joinWithSeparator("")
        return string
    }
    
    
}



