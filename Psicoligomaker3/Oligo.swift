//
//  Oligo.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/23/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//

import Cocoa

class Oligo: NSObject{
    let sequence: String
    let position: Int
    let score: Int
    let geneName: String
    
    init(position: Int, sequence: String, score: Int, geneName: String = ""){
        self.sequence = sequence
        self.position = position
        self.score = score
        self.geneName = geneName
    }
}

struct OligoPair{
    // NB: Oligos are always in 5' -> 3'
    let forwardOligo: String
    let reverseOligo: String
    let position: Int
    let score: Int
    var geneName: String = ""
    
    func summary() -> String {
        return "Gene name: \(self.geneName)\tPosition: \(self.position)\tScore: \(self.score)\nForward:\t\(self.forwardOligo)\nReverse:\t\(self.reverseOligo)\n"
    }
    
    func tableSummary() -> String{
        return ("\(self.geneName).\(self.position)\t\(self.forwardOligo)\t\(self.reverseOligo)\t\(self.score)")
    }
    
    func tableSummaryTwoLines() -> String{
        return ("\(self.geneName).\(self.position).for\t\(self.forwardOligo)\t\(self.score)\n\(self.geneName).\(self.position).rev\t\(self.reverseOligo)\t\(self.score)\n")
    }
    
}
