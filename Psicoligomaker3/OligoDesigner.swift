//
//  OligoDesigner.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 9/3/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//


// an instance of this class takes care of formatting the oligos according to the provided rules.


// Requires String+Sequence.swift extension

import Cocoa



class OligoDesigner: NSObject {
    var topLeftOverhang: String
    var topRightOverhang: String
    var topLoop: String
    var bottomLoop: String
    var bottomLeftOverhang: String
    var bottomRightOverhang: String
    
    init(topLeftOverhang: String, topRightOverhang: String, topLoop: String, bottomLoop: String, bottomLeftOverhang: String, bottomRightOverhang: String){
        self.topLeftOverhang = topLeftOverhang
        self.topRightOverhang = topRightOverhang
        self.topLoop = topLoop
        self.bottomLoop = bottomLoop
        self.bottomLeftOverhang = bottomLeftOverhang
        self.bottomRightOverhang = bottomRightOverhang
    }
    
    func createOligoPair (oligo: Oligo) -> OligoPair{
        // this is the main function: takes a target sequence and generates the corresponding Oligo pair
        var forwardOligo = oligo.sequence
        var reverseOligo = oligo.sequence.complement()
        forwardOligo = self.topLeftOverhang + forwardOligo + self.topLoop + forwardOligo.revCom() + self.topRightOverhang
        reverseOligo = self.bottomLeftOverhang + reverseOligo + self.bottomLoop + reverseOligo.revCom() + self.bottomRightOverhang
        // inverts reverseOligo so that it is in 5' -> 3' orientation
        reverseOligo = reverseOligo.reverse()
        
        return OligoPair(forwardOligo: forwardOligo, reverseOligo: reverseOligo, position: oligo.position, score: oligo.score, geneName: oligo.geneName)
    }
    
}
