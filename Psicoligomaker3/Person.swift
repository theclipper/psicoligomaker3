//
//  Person.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/23/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//

import Foundation

class Person: NSObject {
    dynamic let name = "Andrea"
    dynamic let surname = "Ventura"
}
