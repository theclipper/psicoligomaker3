//
//  ACEShFinder.swift
//  Psicoligomaker3
//
//  Created by Ventura, Andrea/Sloan Kettering Institute on 8/22/16.
//  Copyright © 2016 Ventura, Andrea/Sloan Kettering Institute. All rights reserved.
//


// Instances of this class will take a DNA sequence search for potential shRNAs and calculate their score.


import Cocoa



class ACEShFinder: NSObject {
    let validBases = ["A", "C", "G", "T"]
    var sequence:String
    var sequenceName:String
    init(_ sequence: String, _ sequenceName: String = "") {
        //cleans up the sequence and converts it to Upper case.
        self.sequence = sequence.uppercaseString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        self.sequenceName = sequenceName
    }
    
    func getGCContent(s: String) -> Float{
        //calculates the GC% of s
        var cg = 0
        for c in s.characters{
            if (c == "C" || c == "G"){
                cg = cg + 1
            }
        }
        return Float(cg)/Float(s.characters.count)*100
    }
    
    func calculateScore (oligo: String) -> Int{
        //Calculates the score according to Reynolds et al. Internal repeats not implemented yet.
        var score = 1
        let startIndex = oligo.startIndex
        //var index = startIndex
        // I: 30<GC%<53
        let gcContent = getGCContent(oligo)
        //print (gcContent)
        if  (gcContent > 30 && gcContent <= 53) {
            score = score + 1
            //print ("GC OK!")
        }
        
        // II: number of A's or T's between 15 and 19. Note that the index is zero based, so I am subtracting 1
        var c2 = 0
        for i in 14...17{
            switch oligo[startIndex.advancedBy(i)]{
            case "A", "T":
                c2 = c2 + 1
            default: break
            }
        }
        score = score + c2
        
        //print ("II score is +\(c2)")
        // Debug
        //let c = oligo[index]
        //print ("Index for \(oligo) is \(String(c))")
        
        
        // IV: A at position 19
        //   index = startIndex.advancedBy(18)
        //    if oligo[index]=="A" {
        //         score = score + 2
        //           //print ("IV is +1")
        //        }
        
        switch oligo[startIndex.advancedBy(18)]{
        case "A":
            score = score + 2 // 1 for A 1 for c2
            break
        case "T":
            score = score + 1
            break // this is for c2
        default:
            score = score - 1 // if it is G or C (criterium VII
        }
        
        // VII: no G or C at position 19 (decrease by 1 if not) moving here since index already at 19
        //if (oligo[index] == "G" || oligo[index] == "C") {
        //  score = score - 1
        //print ("VII is -1")
        //}
        
        
        // III: internal hairpins (not implemented yet)
        //score = score + 1
        
        // V: A at position 3
        if oligo[startIndex.advancedBy(2)]=="A" {
            score = score + 1
            //print ("V is +1")
        }
        
        // VI: T at position 10
        
        if oligo[startIndex.advancedBy(9)]=="T" {
            score = score + 1
            //print ("VI is +1")
        }
        
        
        
        // VIII:  G at position 13
        if oligo[startIndex.advancedBy(12)] == "G" {
            score = score - 1
            //print ("VIII is -1")
        }
        
        return score
    }
    
    //consider opportunity to include a check for the presence of specific sites in the oligo.
    func checkValidSequence(s: String) -> Bool
    {
        //if (s[s.startIndex] != "G") {return false}
        if (!s.hasPrefix("G")) {return false}
        
        if (s.containsString("AAAA")) {return false}
        if (s.containsString("TTTT")){ return false}
        
        //        for base in s.characters {
        //            if !validBases.contains(String(base)){
        //                return false
        //            }
        //        }
        return true
    }
    func findSh() -> NSMutableArray {
        //evaluates each 20mer in the sequence and returns an oligos array containing position, sequence, and score
        let oligosArray =  NSMutableArray()
        var index=self.sequence.startIndex
        //var oligosArray:Array<Oligo>
        for i in 1...(self.sequence.characters.count-OLIGOLENGTH) {
            //print("position: \(i)")
            index = index.successor()
            let oligo = self.sequence.substringWithRange(Range<String.Index>(start: index, end: index.advancedBy(OLIGOLENGTH)))
            
            if (self.checkValidSequence(oligo) == false) { // this avoids processing oligos with non conventional nucleotides
                continue
            }
            let score = self.calculateScore(oligo)
            let newEntry = Oligo(position: i, sequence: oligo, score: score, geneName: self.sequenceName)
            oligosArray.addObject(newEntry)
        }
        //for each position calculate score and add entry in dictionary.
        return oligosArray
    }
    
}
